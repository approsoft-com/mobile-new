$(document).ready(function(){

	// Update your server URL
	/*
		NOTE: If you're trying localhost in mobile, it won't work; please use public IP address or host your file in remote server
	*/
	var url="https://app.mysaleshub.co.za/cordova/Server/auth.php?callback=?";
    
    //logout function
    $("#logout").click(function(){
    	localStorage.login="false";
    	window.location.href = "login.html";
    });
	
	//Login Function
    $("#login").click(function(){
    	
    	var email=$("#email").val();
    	var password=$("#password").val();
    	var dataString="email="+email+"&password="+password+"&login=";
    	if($.trim(email).length>0 & $.trim(password).length>0)
		{
			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#login").html('<span style="color:#e85432;">Connecting...</span>');},
				success: function(data){
					if(data == "success")
					{
						localStorage.login="true";
						localStorage.email=email;
						window.location.href = "index.html";
					}
					else {
						if(data == "error"){
							alert("Login error. E-mail or password invalid.");
						}
						else if(data == "deactive"){
							alert("Login error. User is not active.");
						}
						$("#login").html('<span style="color:#e85432;">Login</span>');
					}
				}
			});
		}return false;

    });

    //signup function
    $("#signup").click(function(){
		if (!$('#accept_tc').is(":checked")) {
			alert("Please accept T&C's");
			return;
		}
		if (!$('#age_check').is(":checked")) {
			alert("Please confirm your age");
			return;
		}
    	var fullname=$("#fullname").val();
    	var email=$("#email").val();
		var surname=$("#surname").val();
		var phonenr=$("#phonenr").val();
		var password=$("#password").val();
		var email_tc=$("#email_tc").val();
    	var dataString="fullname="+fullname+"&email_tc="+email_tc+"&surname="+surname+"&phonenr="+phonenr+"&email="+email+"&password="+password+"&signup=";

    	if($.trim(fullname).length>0 & $.trim(email).length>0 & $.trim(password).length>0)
		{
			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#signup").html('<span style="color:#e85432;">Connecting...</span>');},				
				success: function(data){
					if(data == "success")
					{
						alert("Thank you for Registering! You will be redirected to login.");
						window.location.href = "index.html";
					}
					else if(data == "exist")
					{
						alert("You already have an existing account.");
						$("#signup").html('<span style="color:#e85432;">Create an Account</span>');
					}
					else if(data == "error")
					{
						alert("Something Went wrong");
						$("#signup").html('<span style="color:#e85432;">Create an Account</span>');
					}
				}
			});
		}return false;

    });

    //Change Password
    $("#change_password").click(function(){
    	var email=localStorage.email;
    	var old_password=$("#old_password").val();
    	var new_password=$("#new_password").val();
    	var dataString="old_password="+old_password+"&new_password="+new_password+"&email="+email+"&change_password=";
    	if($.trim(old_password).length>0 & $.trim(old_password).length>0)
		{
			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#change_password").val('Connecting...');},
				success: function(data){
					if(data == "incorrect")
					{
						alert("Your old password is incorrect");
					}
					else if(data == "success")
					{
						alert("Password Changed successfully");
					}
					else if(data == "error")
					{
						alert("Something Went wrong");
					}
				}
			});
		}return false;

    });

    //Forget Password
    $("#forget_password").click(function(){
    	var email=$("#email").val();
    	var dataString="email="+email+"&forget_password=";
    	if($.trim(email).length>0)
		{
			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#forget_password").val('Connecting...');},
				success: function(data){
					if(data == "invalid")
					{
						alert("Account not found");
					}
					else if(data == "success")
					{
						alert("A password has been sent to your email address.");
					}
				}
			});
		}return false;

    });
	//Lead Function
    $('#clear-signature').on('click', function(){
        signaturePad.clear();
    });
    $("#submitlead").click(function(){		
		console.log('recevied');
        var imageData = signaturePad.toDataURL();
        document.getElementsByName("image")[0].setAttribute("value", imageData);               
		var action = 'sendlead';
		var repemail = localStorage.email;
		var productid = $("#productid").val();
        var image = $("#image").val();
		var productname = $("#productname").val();
		var productdesc = $("#productdesc").val();
		var productsku = $("#productsku").val();
		var productprice = $("#productprice").val();
		var name = $("#name").val();
		var surname = $("#surname").val();
		var email = $("#email").val();
		var idnr = $("#idnr").val();
		var contactnr = $("#contactnr").val();
        var age_confirmed = '';        
        if ($("#age_check").is(':checked')) {
            age_confirmed = "yes";
        }
        else {
            age_confirmed = "no";
        }                
        if (name == '') {
            alert("Please fill in name");
            return false;
        }
        if (surname == '') {
            alert("Please fill in surname");
            return false;
        }
        if (contactnr == '') {
            alert("Please fill in contact number");
            return false;
		}
		if (!$('#age_check').is(":checked")) {
			alert("Please confirm your age");
			return;
		}
        if (!$('#accept_tc').is(":checked")) {
			alert("Please accept T&C's");
			return;
		}
		
        
        if (image == 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACWCAYAAABkW7XSAAAEYklEQVR4Xu3UAQkAAAwCwdm/9HI83BLIOdw5AgQIRAQWySkmAQIEzmB5AgIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlAABg+UHCBDICBisTFWCEiBgsPwAAQIZAYOVqUpQAgQMlh8gQCAjYLAyVQlKgIDB8gMECGQEDFamKkEJEDBYfoAAgYyAwcpUJSgBAgbLDxAgkBEwWJmqBCVAwGD5AQIEMgIGK1OVoAQIGCw/QIBARsBgZaoSlACBB1YxAJfjJb2jAAAAAElFTkSuQmCC') {
            alert("Please sign");
            return false;
        }		
			$.ajax({
				type: "POST",
				url: 'https://app.mysaleshub.co.za/cordova/Server/display.php',
				data: {action:action,repemail:repemail,productid:productid,productname:productname,productdesc:productdesc,productsku:productsku,productprice:productprice,name:name,surname:surname,email:email,idnr:idnr,contactnr:contactnr,image:image,age_confirmed:age_confirmed},
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#submitlead").html('Sending...');},
				success: function(data){                    
					if(data == "success")
					{
						alert("Success! Your lead has been submitted.");
						window.location.href='index.html';
					}
					else if(data == "error")
					{
						alert("Error! Lead not submitted.");
						window.location.href='index.html';						
					}
				}
			});		
    });
	//end lead
	//Email info Function
    $("#submitemailinfo").click(function(){
		console.log('recevied');
		var action = 'sendemaillead';
		var repemail = localStorage.email;
		var productid = $("#productid").val();
		var productname = $("#productname").val();
		var productdesc = $("#productdesc").val();
		var productsku = $("#productsku").val();
		var productprice = $("#productprice").val();
		var name = $("#name").val();
		var surname = $("#surname").val();
		var email = $("#email").val();
		var idnr = $("#idnr").val();
		var contactnr = $("#contactnr").val();
				
			$.ajax({
				type: "POST",
				url: 'https://app.mysaleshub.co.za/cordova/Server/display.php',
				data: {action:action,repemail:repemail,productid:productid,productname:productname,productdesc:productdesc,productsku:productsku,productprice:productprice,name:name,surname:surname,email:email,idnr:idnr,contactnr:contactnr},
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#submitemailinfo").html('Sending...');},
				success: function(data){
					if(data == "success")
					{
						alert("Success! Your E-mail has been sent.");
						window.location.href='index.html';
					}
					else if(data == "error")
					{
						alert("Error! E-mail not sent.");
						window.location.href='index.html';						
					}
				}
			});		
    });
	//end Emailinfo    

    //Displaying user email on home page
    $("#email1").html(localStorage.email);
    var imageHash="https://www.gravatar.com/avatar/"+md5(localStorage.email);
    $("#profilepic").attr('src',imageHash);
	
	//Unlock Product
    $("#unlockproduct").click(function(){		
		var action = 'unlockproduct';
		var repemail = localStorage.email;
		var productid = $("#productid").val();
		var newUrl = 'single-product.html?id='+productid;	
				
			$.ajax({
				type: "POST",
				url: 'https://app.mysaleshub.co.za/cordova/Server/display.php',
				data: {action:action,repemail:repemail,productid:productid},
				crossDomain: true,
				cache: false,
				beforeSend: function(){ $("#unlockproduct").html('Unlocking...');},
				success: function(data){
					if($.trim(data) == "success")
					{
						alert("Success! You have unlocked this product.");
						window.location.href=newUrl;
					}
					else if($.trim(data) == "error")
					{
						alert("Error! Product not unlocked.");
						window.location.href='index.html';						
					}
				}
			});		
    });
	
	//Close account

    $("#closeaccount").click(function(){
    	
		var confirmfirst = confirm("Are you sure you want to close your account?");
		if (confirmfirst) {
    	var repemail = localStorage.email;
    	var dataString="repemail="+repemail+"&closeaccount=";

			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,				
				success: function(data){
					if($.trim(data) == "success")
					{
						alert("Your account has been deactivated. Please check your e-mail.");
						localStorage.login="false";						
						window.location.href = "login.html";
					}
					else if($.trim(data) == "error")
					{
						alert("An Error has occured.");						
					}
				}
			});
		
	  }
    });
	
	 $("#updatebank").click(function(){
    	
    	var repemail = localStorage.email;
		var bankdetails_ewallet = $("#bankdetails_ewallet").val();
		var banking_option = $("#banking_option").val();
		var bank_name = $("#bank_name").val();
		var account_nr = $("#account_nr").val();
		var branch_name = $("#branch_name").val();
		var branch_code = $("#branch_code").val();
		var account_type = $("#account_type").val();
    	var dataString="bankdetails_ewallet="+bankdetails_ewallet+"&banking_option="+banking_option+"&bank_name="+bank_name+"&account_nr="+account_nr+"&branch_name="+branch_name+"&branch_code="+branch_code+"&account_type="+account_type+"&repemail="+repemail+"&updatebank=";

		if (banking_option == "bank_deposit") {
			if (bank_name == "" ||  account_nr == "" || branch_name == "" || branch_code == "") {
				alert("Please fill in all your banking details");
				return false;
			}
		}
		//if($.trim(bankdetails).length>0)
		//{
			$.ajax({
				type: "POST",
				url: url,
				data: dataString,
				crossDomain: true,
				cache: false,				
				success: function(data){
					if($.trim(data) == "success")
					{
						alert("Success. Your banking details has been updated.");									
						window.location.href = "my-account.html";
					}
					else if($.trim(data) == "error")
					{
						alert("An Error has occured.");						
					}
				}
			});		
			//}else{
				//alert("Bank details can't be empty...");
			//}//return false;
    });
	
	//$("#banking_option").change(function(){
	$(document).on('change', '#banking_option', function() {
		var banking_option = $("#banking_option").val();
		if (banking_option == "ewallet") {
			$("#bankdetails_ewallet").show();
			$("#bankdetails_deposit").hide();
		}
		else {
			$("#bankdetails_ewallet").hide();
			$("#bankdetails_deposit").show();
		}
    });
	
});